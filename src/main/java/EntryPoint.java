import java.io.File;
import java.util.Iterator;
import org.apache.commons.configuration.*;

public class EntryPoint {

    public static void main(String[] args) throws ConfigurationException {
        PropertiesConfiguration propertiesConfig = new PropertiesConfiguration("config.properties");
        XMLConfiguration xmlConfig = new XMLConfiguration("config.xml");
        
        loadConfig(propertiesConfig);
        createConfig("config2.properties", new PropertiesConfiguration());
        
        loadConfig(xmlConfig);
        createConfig("config2.xml", new XMLConfiguration());
    }
    
    public static void loadConfig(Configuration config) throws ConfigurationException {        
        Iterator<String> keys = config.getKeys();
        while (keys.hasNext()) {
            String key = keys.next();
            System.out.println("Klucz: " + key + ", Wartosc: " + config.getString(key));
        }
    }
    
    public static void createConfig(String name, PropertiesConfiguration config) throws ConfigurationException {
        File file = new File(name);
        config.setFile(file);
        appendConfig(config);
        config.save();
    }
    
    public static void appendConfig(Configuration config) throws ConfigurationException {
        config.addProperty("test", "123Test");
    }

    public static void createConfig(String name, XMLConfiguration config) throws ConfigurationException {
        File file = new File(name);
        config.setFile(file);
        appendConfig(config);
        config.save();
    }
}
